<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	"activer" => "Activer",
	"activer_api_key" => "Clé API",
	"api_consult" => "Consultez votre API",
	"aucune" => "Aucune",

	// B
	"build_auto" =>	"Activer le Build automatique",
	"build_auto_desc" => "Via le pipeline post_edition",
	"build_limit" => "Délai avant de pouvoir relancer le build (en min)",

	// C
	"cfg_api" => "Sélectionner les collections à exposer sur l'API",
	"cfg_deploiement" => "Déploiement",
	"cfg_git_clone" => "Cloner un projet",
	"cfg_jeton" => "Sécurisez votre API",
	"cfg_meta" => "Sélectionner les metas à exposer sur l'API",
	"cfg_netlify" => "Netlify",
	"cfg_npm_install" => "Installer les dépendances",
	"champs_objet" => "Sélectionner les champs que vous souhaitez exposer sur l'API",
	"construire" => "Construction du site",
	"copied" => "Copié !!!",
	"copier" => "Copier dans le presse-papier",

	// D
	"deploiement_select" => "Choisissez votre mode de déploiement",
	"dist" => "Dossier de destination du build",
	"dist_desc" => "A l'intérieur du dossier /statique",

	// E
	"erreur_405_message" => "Verifiez la syntaxe de votre requête",
	"erreur_405_titre" => "Méthode non autorisée",
	"error_build_install" => "Veuillez installer les dépendances avant de construire votre site",
	"error_build_timing" => "Veuillez attendre @limit@ minutes avant chaque build.",
	"error_build_title" => "Erreur lors du build",
	"error_clonage_title" => "Erreur lors du clonage du dépôt",
	"error_format_git" => "Erreur dans l'url de votre dépôt",
	"error_format_path" => "Le chemin doit commencer par un slash (/)",
	"error_install_title" => "Erreur lors de l'installation",
	"error_netlify_hook" => "Erreur lors du lancement de la requête.",
	"error_netlify_url" => "Veuillez renseigner l'URL de Hook dans la configuration du plugin.",
	"error_site_absent" => "Veuillez télécharger votre projet avant d'installer ses dépendances.",

	// G
	"generate" => "Re-générer",
	"generated" => "Re-généré !!!",
	"git_clone_ok" => "Un projet Node est présent dans le dossier /statique.",
	"git_remote" => "URL du dépôt",
	"git_remote_desc" => "Votre dépôt doit être publique",
	"git_remote_placeholder" => "https://domain/depot.git ou git@domain:depot.git",

	// I
	"install_ok" => "Les dépendances ont déjà été installées. Souhaitez-vous relancer la commande pour mettre à jour les dépendances ?",

	// L
	"last_build" => "Dernier build",
	"launch_build" => "Lancer le build",
	"launch_build_desc" => "La construction du site peut prendre quelques minutes suivant votre projet",
	"liaisons_desc" => "Sélectionnez les collections liées à exposer",

	// M
	"meta_desc" => "Variables de la table spip_meta",

	// N
	"npm_install_desc" => "Lance la commande <code>npm install</code>",
	"netlify_url" => "Build Hook URL",
	"netlify_url_desc" => "Fournie par Netlify",

	// O
	"objets_editoriaux" => "Objets éditoriaux",
	"options_spip" => "Options SPIP",

	// P
	"pagination" => "Pagination",

	// S
	"select_all" => "Tout sélectionner",
	"shell" => "Commande shell à exécuter",
	"success_build_title" => "Votre site a été construit avec succès !",
	"success_install_title" => "Les dépendances de votre site sont en cours de téléchargement !",
	"success_clonage_title" => "Le clonage du dépôt a réussi !",
	"success_netlify_hook" => "La requête a bien été lancée. Veuillez patienter pendant la construction de votre site ou vérifiez l'état d'avancement dans votre compte Netlify.",

	// T
	"titre_page_configurer_api" => "Données à exposer",
	"titre_configurer_construire" => "Construire le site",
	"titre_configurer_installer" => "Installer le site",
	"titre_configurer_jeton" => "Jeton API",
	"titre_page_configurer_headless" => "SPIP Headless",

);
