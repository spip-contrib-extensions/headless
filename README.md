# SPIP Headless

***/!\ Note importante /!\*** : Ce plugin m'a servi d'entraînement mais peut quand même être utilisé dans l'état. Pour exposer du json vous avez les plugins `collection_json` et `ezest`. Je me suis donc concentré sur [le plugin `graphql`](https://git.spip.net/spip-contrib-extensions/graphql) pour fournir une nouvelle corde à l'arc de l'écureuil ;)

## 1. But

Exposer une API REST pour permettre de développer un site avec un générateur de site statique (Vue.js, Angular, React, Astro, etc.). L'idéal aurait été d'exposer un endpoint GraphQL. En attendant de pouvoir migrer vers cette solution, ce plugin reprend une partie du code et des concepts du plugin `collectionjson` car, malgré son nom, le plugin `ezrest` me paraissait plus difficile à surcharger, même s'il reste facile à utiliser (je l'utilise notamment ici : https://infojune.fr/api_doc/)

## 2. Fonctionnalités

### 2.1 Création d'une table de config dans la BDD (meta_headless)

Définition des champs

### 2.2 Menu de configuration du plugin (?exec=configurer_headless)

- Uniquement pour les Webmestres
- Utilisant la table `meta_headless` pour stocker les valeurs de configuration du plugin
- Affichage sur 3 onglets :

![Formulaire de configuration](./captures/config.png)

- La page _"Données à exposer"_ est affichée par défaut et contient deux parties :

  - Liste des objets éditoriaux avec une case à cocher pour chacun d'eux. Par défaut, rien n'est coché. Si la case est cochée, la liste des champs et des liaisons disponibles s'affichent.
  - Liste des valeurs utiles de la table `meta` avec une case à cocher pour chacune d'elles. Par défaut, rien n'est coché.

- La page _"JETON API"_ contient : - Une case à cocher pour sécuriser ou non les requêtes sur l'API - Si la case est cochée alors : - on génère un jeton stocké dans la table `meta_headless` - on affiche un input[readonly] avec le jeton - Un bouton permet de copier/coller le jeton - Un bouton permet de re-générer le jeton - Si la case est décochée, on supprime le jeton de la table `meta_headless`
  ![Formulaire de jeton API](./captures/jeton.png)
- La page CONSTRUIRE contient :
  - un formulaire pour choisir le mode de déploiement (Pour le moment : local ou [Netlify](https://www.netlify.com/)
  - un formulaire pour lancer le build
    ![Formulaire de construction du site statique](./captures/netlify.png)
- Si le mode de déploiement local est choisi, une page _"Installer le site"_ apparaît et permet de clôner un projet git et de lancer la commande `npm install`.
  ![Formulaire d'installation locale](./captures/installer.png)

### 2.3 Exposer les objets éditoriaux de SPIP au format JSON grâce au plugin `http`

- Prendre en compte le fait que l'API soit sécurisée ou non et dans l'affirmative, vérifier le jeton. Sinon renvoyer une **erreur 403**
- Prendre en compte le fait que la collection ou la meta demandée soit exposée ou non sinon renvoyer une **erreur 404**
  ![Index de l'API](./captures/api.png)

### 2.4 Imposer au webmestre les collections et champs à exposer dans l'API

Pour ça, 3 solutions (dans l'ordre de priorité) :

- Créer les fonctions `http_headless_collection_a_verrouiller_get_collection_dist()` et `http_headless_collection_a_verrouiller_get_ressource_dist()` dans `/http/headless/collection_a_verrouiller.php`
- Créer les squelettes `collection_a_verrouiller.html` et `collection_a_verrouiller-ressource.html` dans `/http/headless/`
- Paramétrer les collections dans le plugin puis ajouter dans `mes_options.php` :

```
define('_HEADLESS_OBJETS_NON_CONFIGURABLES', ['groupes_mots', 'collection_a_verrouiller']);
```

---

**N.B :**
Pour rediriger /http.api/headless vers /spip.php?action=api_http&arg=headless, il faut activer le .htaccess ou :

#### Apache

    RewriteRule ^(ecrire/)?([\w]+)\.api([/.](.*))?$ spip.php?action=api_$2&arg=$4 [QSA,L]

#### NGINX

    rewrite ^(ecrire/)?([\w]+)\.api([/.](.*))?$ spip.php?action=api_$2&arg=$4 last;

---

# Contribuer

Pour contribuer, il est préférable d'avoir une installation locale de SPIP fraîchement installée (avec le minimum de plugins) que vous aurez alimentée avec quelques objets éditoriaux (rubriques, articles, auteurs, documents, mots-clés, etc.).

L'IDE conseillé est VScode.

Pour proposer vos modifications (merge-request) :

```
cd my-projects/spip/plugins
git clone https://git.spip.net/paidge/headless.git
cd headless
git checkout -b my-branch
code .
git commit
git push origin my-branch
```

Puis faites votre demande de fusion.

---

# TODO

## API

- [ ] Tester et corriger les exemples dans `/http/headless`

## Code

- [ ] Factorisation de certaines parties redondantes du code
- [ ] Il y a peut-être des fonctions redondantes avec le coeur de SPIP dans `spip_headless_fonctions.php` (getDatetime, supprimer_dossier, statut_dossier, etc.)
- [ ] Vérifier les chaînes de langue (celles non-utilisées et celles qui seraient redondantes avec le coeur de SPIP)
- [ ] Améliorer les formatters de code pour les fichiers .html
