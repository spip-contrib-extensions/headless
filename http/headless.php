<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('headless_fonctions');
include_spip('action/editer_liens');

/**
 * Contenu d'une erreur
 *
 * @param int $code Le code HTTP de l'erreur à générer
 * @return string Retourne le contenu de l'erreur à renvoyer dans la réponse
 */
function http_headless_erreur_dist($code, $requete, $reponse) {
	$reponse->setStatusCode($code);
	$erreur = ['code' => "$code"];

	switch ($code) {
		case '401':
			$erreur['title'] = _T('http:erreur_401_titre');
			$erreur['message'] = _T('http:erreur_401_message');
			break;
		case '404':
			$erreur['title'] = _T('http:erreur_404_titre');
			$erreur['message'] = _T('http:erreur_404_message');
			break;
		case '405':
			$erreur['title'] = _T('headless:erreur_405_titre');
			$erreur['message'] = _T('headless:erreur_405_message');
			break;
		case '415':
			$erreur['title'] = _T('http:erreur_415_titre');
			$erreur['message'] = _T('http:erreur_415_message');
			break;
		default:
			$erreur = false;
	}

	// Si on reconnait une erreur on l'encapsule dans une collection avec erreur
	if ($erreur) {
		include_spip('inc/filtres');
		$json = headless_prepareAPIjson("", "", $erreur);
		$reponse = headless_prepareAPIreponse($code, $reponse, $json);
	} else {
		$reponse->setContent('');
	}

	return $reponse;
}

/**
 * Index général de l'API
 * http://site/http.api/headless/
 *
 * @param Request $requete    : L'objet Request contenant la requête HTTP
 * @param Response $reponse : L'objet Response qui contiendra la
 *                              réponse envoyée à l'utilisateur
 *
 * @return Response Retourne un objet Response modifié suivant ce
 *                    qu'on a trouvé
 */
function http_headless_get_index_dist($requete, $reponse) {
	// Vérification du jeton API
	if (!headless_verif_jeton($requete)) {
		$fonction_erreur = charger_fonction('erreur', "http/headless/");
		$reponse = $fonction_erreur(403, $requete, $reponse);
		return $reponse;
	}

	include_spip('base/objets');
	include_spip('inc/autoriser');
	include_spip('inc/config');

	$links = [];
	$collections_objets = lire_config("/meta_headless/objets_editoriaux", []);
	foreach (lister_tables_objets_sql() as $table => $desc) {
		$collection = table_objet($table);
		if (
			autoriser('get_collection', $collection) &&
			(array_key_exists($collection, $collections_objets) ||
				find_in_path("http/headless/" . $collection . ".html"))
		) {
			$filtres = array_merge(
				lire_config("/meta_headless/objets_editoriaux/" . $collection . "/champs", []),
				["date_min", "date_max", "recherche"]
			);

			$links[] = [
				'rel' => 'collection',
				'name' => $collection,
				'prompt' => _T($desc['texte_objets']),
				'href' => rtrim(url_absolue(self('&')), '/') . '/' . $collection . '/',
				'filters' => $filtres
			];
		}
	}

	$collections_meta = lire_config("/meta_headless/meta", []);
	if ($collections_meta) {
		$links[] = [
			'rel' => 'collection',
			'name' => "meta",
			'prompt' => _T("headless:meta_desc"),
			'href' => rtrim(url_absolue(self('&')), '/') . '/meta/'
		];
	}

	$json = headless_prepareAPIjson($links);
	headless_prepareAPIreponse(200, $reponse, $json);

	return $reponse;
}

/**
 * GET sur une collection
 * http://site/http.api/headless/patates
 *
 * @param Request $requete L'objet Request contenant la requête HTTP
 * @param Response $reponse L'objet Response qui contiendra la réponse envoyée à l'utilisateur
 * @return Response Retourne un objet Response modifié suivant ce qu'on a trouvé
 */
function http_headless_get_collection_dist($requete, $reponse) {
	// Vérification du jeton API
	if (!headless_verif_jeton($requete)) {
		$fonction_erreur = charger_fonction('erreur', "http/headless/");
		$reponse = $fonction_erreur(403, $requete, $reponse);
		return $reponse;
	}

	$collections_autorisees = lire_config('/meta_headless/objets_editoriaux', []);
	$collection_demandee = $requete->attributes->get('collection');
	$format = $requete->attributes->get('format');
	$contexte = $requete->query->all();

	// S'il s'agit de la collection meta
	if ($collection_demandee == "meta") {
		$metas = lire_config("/meta_headless/meta", []);
		$where = "nom IN ('" . implode("','", $metas) . "')";
		$liste_meta_publique = sql_allfetsel("nom, valeur", "spip_meta", $where);

		if (!empty($liste_meta_publique)) {
			$items = [];
			foreach ($liste_meta_publique as $meta) {
				$items[] = ["nom" => $meta["nom"], "valeur" => $meta["valeur"]];
			}
			$json = headless_prepareAPIjson([], $items);
			$reponse = headless_prepareAPIreponse(200, $reponse, $json);
		} else {
			$fonction_erreur = charger_fonction('erreur', 'http/headless/');
			$reponse = $fonction_erreur(404, $requete, $reponse);
		}
	}
	// Si aucune collection n'est exposée
	elseif (empty($collections_autorisees)) {
		$fonction_erreur = charger_fonction('erreur', 'http/headless/');
		$reponse = $fonction_erreur(404, $requete, $reponse);
	}
	// S'il existe une fonction globale, dédiée à ce type de ressource, qui gère TOUTE la requête, on n'utilise QUE ça
	// Cette fonction doit donc évidemment renvoyer un objet Response valide
	elseif ($fonction_collection = charger_fonction('get_collection', "http/$format/$collection_demandee/", true)) {
		$reponse = $fonction_collection($requete, $reponse);
	}
	// Si un squelette existe
	elseif ($json = recuperer_fond("http/headless/$collection_demandee", $contexte)) {
		$reponse = headless_prepareAPIreponse(200, $reponse, $json);
		// Sinon on essaye de trouver différentes méthodes pour produire le JSON
	} else {
		$json = headless_get_collection($collection_demandee, $contexte);
		if (!empty($json)) {
			$reponse = headless_prepareAPIreponse(200, $reponse, $json);
		} else {
			$fonction_erreur = charger_fonction('erreur', 'http/headless/');
			$reponse = $fonction_erreur(404, $requete, $reponse);
		}
	}

	return $reponse;
}

/*
 * GET sur une ressource
 * http://site/http.api/headless/patates/1234
 *
 * @param Request $requete L'objet Request contenant la requête HTTP
 * @param Response $reponse L'objet Response qui contiendra la réponse envoyée à l'utilisateur
 * @return Response Retourne un objet Response modifié suivant ce qu'on a trouvé
 */
function http_headless_get_ressource_dist($requete, $reponse) {
	// Vérification du jeton API
	if (!headless_verif_jeton($requete)) {
		$fonction_erreur = charger_fonction('erreur', "http/headless/");
		$reponse = $fonction_erreur(403, $requete, $reponse);
		return $reponse;
	}

	$format = $requete->attributes->get('format');
	$collection = $requete->attributes->get('collection');

	// S'il existe une fonction globale, dédiée à ce type de ressource, qui gère TOUTE la requête, on n'utilise QUE ça
	// Cette fonction doit donc évidemment renvoyer un objet Response valide
	if ($fonction_ressource = charger_fonction('get_ressource', "http/$format/$collection/", true)) {
		$reponse = $fonction_ressource($requete, $reponse);
	}
	// Sinon on essaye de trouver différentes méthodes pour produire le JSON et en déduire les headers :
	// - par un squelette
	// - par un échafaudage générique
	else {
		$json = [];

		// Pour l'instant on va simplement chercher un squelette du type de la ressource
		// Le squelette prend en contexte les paramètres du GET + l'identifiant de la ressource en essayant de faire au mieux
		include_spip('base/objets');
		$ressource = $requete->attributes->get('ressource');
		$cle = id_table_objet($collection);
		$contexte = [
			$cle => $ressource,
			'ressource' => $ressource,
		];
		$contexte = array_merge($requete->query->all(), $requete->attributes->all(), $contexte);

		if ($skel = trim(recuperer_fond("http/$format/$collection-ressource", $contexte))) {
			$json = json_decode($skel, true);
		}

		// Si on n'a toujours aucun contenu json, et que la collection est bien autorisée,
		// on en échafaude un avec les API d'objets
		$collections_autorisees = lire_config('/meta_headless/objets_editoriaux');
		if (empty($json) && array_key_exists($collection, $collections_autorisees)) {
			$objet = objet_type($collection); // Singulier
			$id_objet = intval($ressource);
			$item = headless_get_objet($objet, $id_objet);

			if (!empty($item)) {
				$json = headless_prepareAPIjson("", [$item]);
			}
		}

		if (!empty($json)) {
			$reponse = headless_prepareAPIreponse(200, $reponse, $json);
		} else {
			$fonction_erreur = charger_fonction('erreur', "http/$format/");
			$reponse = $fonction_erreur(404, $requete, $reponse);
		}
	}

	return $reponse;
}
