<?php

/**
 * Fichier gérant l'installation et désinstallation du plugin SPIP Headless
 *
 * @plugin     SPIP Headless
 * @copyright  2023
 * @author     Pierre-jean CHANCELLIER
 * @licence    GNU/GPL v3
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction d'installation et de mise à jour du plugin
 *
 * Vous pouvez :
 *
 * - créer la structure SQL,
 * - insérer du pre-contenu,
 * - installer des valeurs de configuration,
 * - mettre à jour la structure SQL
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
 **/
function headless_upgrade($nom_meta_base_version, $version_cible) {
	$maj = array();
	// Création de la table qui stockera la config du plugin
	$maj['create'][] = array('installer_table_meta', 'meta_headless');

	// Enregistrement de valeurs par défaut dans la table de config du plugin
	$maj['create'][] = array('ecrire_config', '/meta_headless/build_limit', 10);
	$maj['create'][] = array('ecrire_config', '/meta_headless/last_build', "0000-00-00");
	$maj['create'][] = array('ecrire_config', '/meta_headless/netlify_url', '');
	$maj['create'][] = array('ecrire_config', '/meta_headless/choix_deploiement', 'local');
	$maj['create'][] = array('ecrire_config', '/meta_headless/jeton', '');
	$maj['create'][] = array('ecrire_config', '/meta_headless/depot_git', '');
	$maj['create'][] = array('ecrire_config', '/meta_headless/cmd_build_static', 'npm run build');

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Fonction de désinstallation du plugin.
 *
 * Vous devez :
 *
 * - nettoyer toutes les données ajoutées par le plugin et son utilisation
 * - supprimer les tables et les champs créés par le plugin.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
 **/
function headless_vider_tables($nom_meta_base_version) {
	// Suppression de la table qui stocke la config du plugin
	sql_drop_table('spip_meta_headless');
	// Effacement de la version du schema du plugin dans la table spip_meta
	effacer_meta($nom_meta_base_version);
}
