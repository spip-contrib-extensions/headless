<?php

/**
 * Utilisations de pipelines
 *
 * @plugin     SPIP Headless
 * @copyright  2023
 * @author     Pierre-jean CHANCELLIER
 * @licence    GNU/GPL v3
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// Permet de surcharger le CSS/JS de l'espace privé
function headless_header_prive($flux) {
	$script_headless_build = recuperer_fond('prive/js/headless');
	$flux .= $script_headless_build;
	return $flux;
}

// Affiche qqe chose dans la colonne de gauche
function headless_affiche_gauche($flux) {
	include_spip('inc/presentation');

	if (strpos($flux['args']['exec'], 'configurer_headless') !== false) {
		$jeton = lire_config('/meta_headless/jeton', "");
		$choix_deploiement = lire_config('/meta_headless/choix_deploiement', "");

		$flux['data'] .=
			debut_cadre_relief('', true, '', _T('headless:api_consult')) .
			recuperer_fond('prive/squelettes/headless_lien', array('text' => 'Go', 'url' => '/http.api/headless')) .
			fin_cadre_relief(true);

		if (!empty($jeton)) {
			$flux['data'] .=
				debut_cadre_relief('', true, '', _T('headless:titre_configurer_jeton')) .
				$jeton .
				fin_cadre_relief(true);
		}

		$flux['data'] .=
			debut_cadre_relief('', true, '', _T('headless:cfg_deploiement')) .
			strtoupper($choix_deploiement) .
			fin_cadre_relief(true);
	}
	return $flux;
}


function headless_formulaire_traiter($flux) {
	$form = $flux['args']['form'];
	$collections_autorisees = lire_config('/meta_headless/objets_editoriaux', []);

	// Si on n'est pas sur un formulaire d'édition et que la collection n'est pas autorisée
	if (!(preg_match("#^editer_(\w+)$#", $form, $matches)
		&& ($collection_modifiee = table_objet($matches[1]))
		&& array_key_exists($collection_modifiee, $collections_autorisees))) {
		return $flux;
	}

	// Si le Build auto est activé et que les champs modifiés de la collection sont exposés dans l'API
	$build_auto = lire_config('/meta_headless/build_auto');
	if (
		$build_auto[0] == 'oui'
		&& array_key_exists($collection_modifiee, $collections_autorisees)
	) {

		$champs_autorises = lire_config('/meta_headless/objets_editoriaux/' . $collection_modifiee . '/champs', []);
		$lancer_build = false;
		foreach ($flux['data'] as $champ_modifie => $valeur) {
			if (in_array($champ_modifie, $champs_autorises)) {
				$lancer_build = true;
			}
		}

		if (!$lancer_build) return $flux;

		$retour = headless_lancer_build();

		if (!empty($retour['message_erreur'])) {
			// Cela veut dire que le build a été fait il y a peu de temps (voir config)
			// Peut-être mettre la tâche dans une tâche planifiée SPIP ?
			// Si la tâche est encore en attente de traitement, ne pas la rajouter dans la pile
		}
	}

	return $flux;
}
