<?php

/**
 * Options
 *
 * @plugin     SPIP Headless
 * @copyright  2023
 * @author     Pierre-jean CHANCELLIER
 * @licence    GNU/GPL v3
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// Dossier de destination du code source du site statique
const _DIR_HEADLESS_SRC = "statique";

// Définition du dossier contenant les squelettes du plugin
// $GLOBALS['dossier_squelettes'] = _DIR_PLUGIN_HEADLESS . 'squelettes';

// Pour débugger pendant le développement
// define('_LOG_FILTRE_GRAVITE', 8);
