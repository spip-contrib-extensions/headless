<?php
function formulaires_lancer_build_charger_dist() {
	$valeurs["last_build"] = lire_config('/meta_headless/last_build', "");

	return $valeurs;
}

function formulaires_lancer_build_traiter_dist() {
	return headless_lancer_build();
}
