<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('base/objets');
define('_HEADLESS_OBJETS_NON_CONFIGURABLES', []);

function formulaires_configurer_collections_saisies_dist() {
	$collections_non = array_merge(_HEADLESS_OBJETS_NON_CONFIGURABLES, ['depots', 'paquets', 'plugins']);
	$saisies = [];
	$tables = lister_tables_objets_sql();
	ksort($tables);
	foreach ($tables as $table => $infos) {
		$collection = $infos['table_objet'];
		if (sql_countsel($table) == '0') {
			array_push($collections_non, $collection);
		}

		if (!in_array($collection, $collections_non)) {
			$champs = [];
			foreach ($infos["field"] as $nom_champ => $def) {
				$champs[$nom_champ] = $nom_champ;
			}

			// On n'affiche que les tables auxiliaires finissant par "_liens"
			// Qui sont autorisées et qui ont du contenu lié
			$collections_autorisees = lire_config("/meta_headless/objets_editoriaux", []);
			$collections_liees = [];
			foreach (lister_tables_auxiliaires() as $table_aux => $infos_aux) {
				if (
					preg_match("#^spip_(\w+)_liens$#", $table_aux, $matches) &&
					array_key_exists($matches[1], $collections_autorisees) &&
					sql_countsel($table_aux, "objet='" . objet_type($table) . "'") != 0
				) {
					$collections_liees[$matches[1]] = $matches[1];
				}
			}

			if (
				!charger_fonction('get_collection', "http/headless/" . $collection . "/", true) &&
				!find_in_path("http/headless/" . $collection . ".html")
			) {
				$saisies_fieldset = [
					[
						'saisie' => 'checkbox',
						'options' => [
							'nom' => $collection . '_exposer',
							'data' => [
								'actif' => 'Actif',
							],
						],
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => $collection . '_pagination',
							'explication' => _T('headless:pagination'),
							'type' => 'number',
							'defaut' => '10',
							'afficher_si' => '@' . $collection . '_exposer@=="actif"',
						],
					],
					[
						'saisie' => 'selection_multiple',
						'options' => [
							'nom' => $collection . '_champs',
							'explication' => _T('headless:champs_objet'),
							'afficher_si' => '@' . $collection . '_exposer@=="actif"',
							'data' => $champs,
							'multiple' => 'oui',
							"cacher_option_intro" => "oui",
							"defaut" => id_table_objet($table),
							'size' => 5,
						],
					],
				];

				if (!empty($collections_liees)) {
					$saisies_fieldset[] = [
						'saisie' => 'selection_multiple',
						'options' => [
							'nom' => $collection . '_liaisons',
							'explication' => _T('headless:liaisons_desc'),
							'afficher_si' => '@' . $collection . '_exposer@=="actif"',
							'data' => $collections_liees,
							'multiple' => 'oui',
							'option_intro' => _T('headless:aucune'),
							'size' => 5,
						],
					];
				}

				$saisies_fieldset = array_merge($saisies_fieldset, [
					[
						'saisie' => 'select_all',
						'options' => [
							'nom' => $collection . '_select_all',
							'afficher_si' => '@' . $collection . '_exposer@=="actif"',
						],
					],
					[
						'saisie' => 'submit',
						'options' => [
							'nom' => $collection . '_submit',
							'conteneur_class' => 'save_api_btn',
						],
					],
				]);

				$saisies[$collection] = [
					'saisie' => 'fieldset',
					'options' => [
						'nom' => $collection,
						'label' => ucfirst($collection),
					],
					'saisies' => $saisies_fieldset,
				];
			}
		}
	}

	return $saisies;
}

function formulaires_configurer_collections_charger_dist() {
	$valeurs = [];
	$objets_editoriaux = lire_config('/meta_headless/objets_editoriaux', array());

	foreach ($objets_editoriaux as $collection => $statut) {
		$valeurs[$collection . "_exposer"] = "actif";
		$valeurs[$collection . "_pagination"] = $statut["pagination"];
		$valeurs[$collection . "_champs"] = $statut["champs"];
		$valeurs[$collection . "_liaisons"] = $statut["liaisons"];
	}
	return $valeurs;
}

function formulaires_configurer_collections_traiter_dist() {
	$ret = [];
	$objets_editoriaux = [];
	foreach (lister_tables_objets_sql() as $table => $infos) {
		$collection = $infos['table_objet'];
		if (_request($collection . '_exposer')) {
			$objets_editoriaux[$collection] = [
				'pagination' => _request($collection . '_pagination'),
				'champs' => _request($collection . '_champs'),
				'liaisons' => _request($collection . '_liaisons'),
			];
		}
	}

	if (ecrire_config('/meta_headless/objets_editoriaux', $objets_editoriaux)) {
		$ret['message_ok'] = _T('config_info_enregistree');
	} else {
		$ret['message_erreur'] = _T('erreur_technique_enregistrement_impossible');
	}

	return $ret;
}
