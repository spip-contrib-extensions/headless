<?php
function formulaires_npm_install_traiter_dist() {
	if (file_exists(_DIR_RACINE . _DIR_HEADLESS_SRC . "/package.json")) {
		$output = array();
		$retour = 0;
		// fini les timeout. COmment vérifier que le processus est terminé pour autoriser le build ?
		sleep(10);
		$commande = 'cd ' . _DIR_RACINE . _DIR_HEADLESS_SRC . " && npm i";
		exec($commande, $output, $retour);

		if ($retour !== 0) {
			$ret['message_erreur'] = "<strong>" . _T('headless:error_install_title') . "</strong><br>" . implode("<br>", $output);
		} else {
			$ret['message_ok'] = "<strong>" . _T('headless:success_install_title') . "</strong><br>" . implode("<br>", $output);
		}
	} else {
		$ret['message_erreur'] = "<strong>" . _T('headless:error_site_absent');
	}

	return $ret;
}
