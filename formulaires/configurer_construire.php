<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function formulaires_configurer_construire_saisies_dist() {
	$saisies = [];
	$saisies[] = array(
		'saisie' => 'input',
		'options' => array(
			'nom' => 'cmd_build_static',
			'label' => _T('headless:shell'),
			'obligatoire' => 'oui',
			'explication' => _T('headless:dist_desc'),
		),
	);

	$saisies[] = array(
		'saisie' => 'checkbox',
		'options' => array(
			'nom' => 'build_auto',
			'label' => _T('headless:build_auto'),
			'explication' => _T('headless:build_auto_desc'),
			'data' => array(
				'oui' => 'Oui',
			),
		),
	);

	return $saisies;
}

function formulaires_configurer_construire_charger_dist() {
	$valeurs = array();
	$valeurs["build_limit"] = lire_config('/meta_headless/build_limit', 10);
	$valeurs["cmd_build_static"] = lire_config('/meta_headless/cmd_build_static');
	$valeurs["build_auto"] = lire_config('/meta_headless/build_auto');

	return $valeurs;
}

function formulaires_configurer_construire_traiter_dist() {
	$ret = array();
	$cmd = _request("cmd_build_static");
	$build_limit = _request("build_limit");
	// $build_auto = _request("build_auto", []);
	$build_auto = _request("build_auto") ? _request("build_auto") : "";

	if (!(ecrire_config('/meta_headless/cmd_build_static', $cmd)
		&& ecrire_config('/meta_headless/build_limit', $build_limit)
		&& ecrire_config('/meta_headless/build_auto', $build_auto)
	)) {
		$ret['message_erreur'] = _T('erreur_technique_enregistrement_impossible');
	}

	$ret['message_ok'] = _T('config_info_enregistree');

	return $ret;
}
