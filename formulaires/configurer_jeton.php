<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function formulaires_configurer_jeton_saisies_dist() {
	$saisies = [];
	$saisies[] = array(
		'saisie' => 'checkbox',
		'options' => array(
			'nom' => 'api_key_active',
			'label' => _T('headless:activer_api_key'),
			'data' => array(
				'oui' => _T('headless:activer'),
			),
		),
	);

	$saisies[] = array(
		'saisie' => 'input',
		'options' => array(
			'nom' => "jeton",
			'label' => _T('headless:titre_configurer_jeton'),
			'readonly' => 'oui',
			'afficher_si' => '@api_key_active@ == "oui"',
			'afficher_si_avec_post' => 'oui',
		),
	);

	return $saisies;
}

function formulaires_configurer_jeton_charger_dist() {
	$valeurs = array();
	$jeton = lire_config('/meta_headless/jeton', "");

	if (empty($jeton)) {
		$valeurs["api_key_active"] = "non";
		$valeurs["jeton"] = headless_generate_key();
	} else {
		$valeurs["api_key_active"] = "oui";
		$valeurs["jeton"] = $jeton;
	}

	return $valeurs;
}

function formulaires_configurer_jeton_traiter_dist() {
	$ret = array();
	$choix = _request("api_key_active") ? _request("api_key_active") : "";
	$jeton = ($choix) ? _request("jeton") : "";

	if (ecrire_config('/meta_headless/jeton', $jeton)) {
		$ret['message_ok'] = _T('config_info_enregistree');
	} else {
		$ret['message_erreur'] = _T('erreur_technique_enregistrement_impossible');
	}
	// $ret['editable'] = true;

	return $ret;
}
