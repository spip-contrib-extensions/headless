<?php
function formulaires_git_clone_saisies_dist() {
	$saisies = [];

	$saisies[] = array(
		'saisie' => 'input',
		'options' => array(
			'nom' => "depot_git",
			'label' => _T('headless:git_remote'),
			'placeholder' => _T('headless:git_remote_placeholder'),
			'obligatoire' => 'oui',
			'explication' => _T('headless:git_remote_desc'),
		),
		'verifier' => array(
			'type' => 'regex',
			'options' => array(
				'modele' => '#^(http(s)?:\/\/([a-zA-Z0-9@:%_\+~-]{1,256}\.){1,2}[a-z]{1,6}\/|git@([a-zA-Z0-9@:%_\+~-]{1,256}\.){1,2}[a-z]{1,6}:)[a-zA-Z0-9@:%_\+~-]{1,256}\/[a-zA-Z0-9@:%_\+~-]{1,256}.git$#',
				'message_erreur' => _T('headless:error_format_git'),
			),
		),
	);

	return $saisies;
}

function formulaires_git_clone_charger_dist() {
	$valeurs["depot_git"] = lire_config('/meta_headless/depot_git');
	return $valeurs;
}

function formulaires_git_clone_traiter_dist() {
	$depot_git = _request("depot_git");
	$ret = array();

	if (!ecrire_config('/meta_headless/depot_git', $depot_git)) {
		$ret['message_erreur'] = _T('erreur_technique_enregistrement_impossible');
		return $ret;
	}

	$statut_dossier_statique = headless_statut_dossier(_DIR_RACINE . _DIR_HEADLESS_SRC);
	$output = array();
	$retour = 0;
	switch ($statut_dossier_statique) {
		case "nonvide":
			headless_supprimer_dossier(_DIR_RACINE . _DIR_HEADLESS_SRC);
		case "inexistant":
			mkdir(_DIR_RACINE . _DIR_HEADLESS_SRC);
		case "vide":

			$commande = 'cd ' . _DIR_RACINE . _DIR_HEADLESS_SRC . " && git clone " . $depot_git . " .";
			exec($commande, $output, $retour);
	}

	if ($retour !== 0) {
		$ret['message_erreur'] = _T('headless:error_clonage_title') . "<br>" . implode("<br>", $output);
	} else {
		$ret['message_ok'] = _T('headless:success_clonage_title') . "<br>" . implode("<br>", $output);
	}

	return $ret;
}
