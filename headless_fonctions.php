<?php

/**
 * Fonctions utiles au plugin
 *
 * @plugin     SPIP Headless
 * @copyright  2023
 * @author     Pierre-jean CHANCELLIER
 * @licence    GNU/GPL v3
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// Vérification du jeton API
function headless_verif_jeton($requete) {
	include_spip("/inc/config");
	$token_param = lire_config('/meta_headless/jeton', '');
	if ($token_param == '') {
		return true;
	}

	$headers = $requete->server->getHeaders();
	$token = '';
	if (isset($headers["X_AUTH_TOKEN"])) {
		$token = $headers["X_AUTH_TOKEN"];
	}

	return ($token == $token_param);
}

// Générer un jeton API
function headless_generate_key() {
	$d = time() * 1000;

	if (function_exists('hrtime')) {
		$hrtime = hrtime();
		$d += round($hrtime[1] / 1000000);
	} elseif (function_exists('microtime')) {
		$microtime = microtime(true);
		$d += round($microtime * 1000);
	}

	$uuid = sprintf('%08s-%04s-4%03x-%04x-%012s', substr($d, -8), substr($d, -12, 4), mt_rand(0, 0xfff), mt_rand(0, 0x3fff) | 0x8000, bin2hex(random_bytes(6)));

	return $uuid;
}

// Statut d'un dossier ("vide" | "nonvide" | "inexistant")
function headless_statut_dossier($dossier) {
	if (!is_dir($dossier)) {
		return "inexistant";
	}

	$fichierTrouve = 0;

	if ($dh = opendir($dossier)) {
		while (($file = readdir($dh)) !== false && $fichierTrouve == 0) {
			if ($file != "." && $file != "..") {
				$fichierTrouve = 1;
			}
		}
		closedir($dh);
	}

	return ($fichierTrouve == 0) ? "vide" : "nonvide";
}

// Supprime un dossier et son contenu
function headless_supprimer_dossier($dossier) {
	if (!is_dir($dossier)) {
		return;
	}

	if ($dh = opendir($dossier)) {
		while (($file = readdir($dh)) !== false) {

			if ($file == "." || $file == "..") {
				continue;
			}

			if (is_dir($dossier . "/" . $file)) {
				headless_supprimer_dossier($dossier . "/" . $file);
			} else {
				chmod($dossier . "/" . $file, 0755);
				unlink($dossier . "/" . $file);
			}
		}
		closedir($dh);
	}
	rmdir($dossier);
}

// Récupère la date et l'heure du moment
function headless_getDatetime($value = "") {
	$dtz = new DateTimeZone('Europe/Paris');
	$datetime = ($value == "") ? new DateTime("now", $dtz) : new DateTime($value, $dtz);
	return $datetime;
}

// Barre à onglets personnalisée
function headless_barre_onglets($rubrique, $ongletCourant, $class = 'barre_onglet') {
	include_spip('inc/presentation');
	$deploiement = lire_config('/meta_headless/choix_deploiement');
	$res = '';

	foreach (definir_barre_onglets($rubrique) as $exec => $onglet) {
		$url = $onglet->url ?: generer_url_ecrire($exec);

		if (
			$rubrique != "configurer_headless" ||
			$exec != "configurer_headless_installer" ||
			$deploiement != "netlify"
		) {
			$res .= onglet(_T($onglet->libelle), $url, $exec, $ongletCourant, $onglet->icone);
		}
	}

	return !$res ? '' : (debut_onglet($class) . $res . fin_onglet());
}

/**
 * Produit le contenu du JSON d'une collection
 * - par un squelette
 * - par un échafaudage générique
 *
 * @param string $collection Nom de la collection à générer
 * @param array $contexte Tableau associatif de l'environnement (à priori venant du GET)
 * @return array Retourne un tableau associatif représentant la collection ou un tableau vide si erreur (générera une 404)
 **/
function headless_get_collection($collection, $contexte) {
	$json = [];
	include_spip('base/abstract_sql');
	include_spip('base/objets');

	$collections_autorisees = lire_config('/meta_headless/objets_editoriaux/');
	if (!array_key_exists($collection, $collections_autorisees)) return [];

	$offset = isset($contexte['offset']) ? $contexte['offset'] : 0;
	$table_collection = table_objet_sql($collection);
	$table_infos = lister_tables_objets_sql($table_collection);
	$pagination = lire_config('/meta_headless/objets_editoriaux/' . $collection . '/pagination', 50);
	$cle_objet = id_table_objet($table_collection);
	$champs_config = lire_config('/meta_headless/objets_editoriaux/' . $collection . '/champs', [$cle_objet]);

	// paramètres de la requete
	$where = [];
	foreach ($contexte as $param_name => $param_value) {
		$is_filter = false;
		$signe = "=";

		switch (true) {
			case (array_key_exists($param_name, $table_infos["field"])):
				$is_filter = true;
				break;
			case ($param_name == 'date_min'):
			case ($param_name == 'date_max'):
				$is_filter = true;
				// Fonctionne avec le format suivant `AAAA-MM-JJ HH:MM:SS`
				$param_name = "maj";
				$signe = ($param_name === "date_min") ? "<" : ">";
				break;
			case (in_array($param_name, ['recherche', 'arg', 'action', 'offset'])):
				break;
			default:
				return '';
		}

		if ($is_filter) {
			$where[] = 'collection.' . $param_name . " " . $signe . sql_quote($param_value);
		}
	}

	if ($collection != "auteurs" && array_key_exists("statut", $table_infos["field"])) {
		$where[] = "collection.statut=" . sql_quote('publie');
	}

	if (empty($contexte['recherche'])) {
		$lignes = sql_allfetsel($champs_config, $table_collection . ' as collection', $where, '', '', "$offset,$pagination");
		$nb_objets = sql_countsel($table_collection);
	} else {
		$lignes = headless_get_recherche($table_collection, $contexte['recherche'], $where);
		$nb_objets = count($lignes);
		$lignes = array_slice($lignes, $offset, $pagination);
	}


	$items = array();
	foreach ($lignes as $champs) {
		$items[] = headless_get_objet(objet_type($table_collection), $champs[$cle_objet], $champs);
	}

	// On génère la pagination si besoin
	$links = array();

	if ($offset > 0) {
		$offset_precedant = max(0, $offset - $pagination);
		$links[] = array(
			'rel' => 'prev',
			'prompt' => _T('public:page_precedente'),
			'href' => url_absolue(
				parametre_url(self('&'), 'offset', $offset_precedant, '&')
			),
		);
	}

	if (($offset + $pagination) < $nb_objets) {
		$offset_suivant = $offset + $pagination;
		$links[] = array(
			'rel' => 'next',
			'prompt' => _T('public:page_suivante'),
			'href' => url_absolue(
				parametre_url(self('&'), 'offset', $offset_suivant)
			),
		);
	}

	$json = headless_prepareAPIjson($links, $items);
	return $json;
}

/**
 * Vue générique d'un objet en JSON
 *
 * Cette fonction sert à mutualiser le code d'échafaudage pour générer le GET d'un objet.
 *
 * @param string $objet Type de l’objet dont on veut générer la vue
 * @param int $id_objet Identifiant de l’objet dont on veut générer la vue
 * @param string $contenu Optionnellement, les champs SQL déjà récupérés de l’objet, pour éviter de faire une requête
 * @return array Retourne un tableau associatif représentant la ressource
 */
function headless_get_objet($objet, $id_objet, $champs = []) {
	include_spip('inc/filtres');

	$collection = table_objet($objet); // l'objet au pluriel
	$table_sql = table_objet_sql($objet);

	$collections_autorisees = lire_config("/meta_headless/objets_editoriaux");
	$config_collection = $collections_autorisees[$collection];
	$champs_config = $config_collection['champs'];

	// On évite de faire à nouveau une requête
	if (empty($champs)) {
		$where = [id_table_objet($objet) . ' = ' . intval($id_objet)];
		// Si on est pas sur la table auteurs et que le champ statut existe dans la table, on rajoute un WHERE statut="publie"
		if ($collection != "auteurs" && array_key_exists("statut", lister_tables_objets_sql($table_sql)["field"])) {
			$where[] = "statut='publie'";
		}
		$champs = sql_fetsel($champs_config, $table_sql, $where);
	}

	if (empty($champs)) {
		return [];
	}

	// Initialisation de la valeur de retour
	$data = ['href' => url_absolue("http.api/headless/$collection/$id_objet")];

	// Champ de base pour créer le champ slug
	switch ($collection) {
		case "auteurs":
			$slug_origin = "nom";
			break;
		case "syndic":
			$slug_origin = "nom_site";
			break;
		default:
			$slug_origin = "titre";
	}

	// Récupération des champs à retourner
	foreach ($champs as $champ => $valeur) {
		switch ($champ) {
			case "titre":
				$data[$champ] = supprimer_numero($valeur);
				$data['rang'] = (preg_match("#^([0-9]+)[.][[:space:]]#", $valeur, $matches)) ? $matches[1] : 0;
				break;
			case "fichier":
				$data[$champ] = url_absolue(_DIR_IMG . $valeur);
				break;
			case "saisies":
				// TODO : transformer le tableau de saisies en html
				$data[$champ] = $valeur;
				break;
			default:
				$data[$champ] = liens_absolus(trim(ptobr(propre($valeur))));
				break;
		}

		if ($champ == $slug_origin) {
			$data["slug"] = identifiant_slug(supprimer_numero($valeur));
		}
	}



	// Récupération du logo de l'objet s'il existe
	$chercher_logo = charger_fonction('chercher_logo', 'inc');
	if ($logo = $chercher_logo($id_objet, $objet, 'on')) {
		if (!is_null($logo[0])) {
			$data['logo'] = lire_config("adresse_site") . "/" . $logo[0];
		}
	}

	// Affichage des collections liées
	$liaisons_config = (array_key_exists('liaisons', $config_collection) &&
		is_array($config_collection['liaisons'])) ? $config_collection['liaisons'] : [];


	foreach ($liaisons_config as $collection) {
		$table = table_objet_sql($collection);
		$type = objet_type($table); // Au singulier
		$cle_collection = id_table_objet($table);

		if (
			array_key_exists($collection, $collections_autorisees) &&
			$liaison_col = objet_trouver_liens([$type => '*'], [$objet => $id_objet])
		) {

			foreach ($liaison_col as $l) {
				$ids[] = $l[$cle_collection];
			}

			$champs_autorises = $collections_autorisees[$collection]['champs'];
			$where = [sql_in($cle_collection, $ids)];
			// Si on est pas sur la table auteurs et que le champ statut existe dans la table, on rajoute un WHERE statut="publie"
			if ($collection != "auteurs" && array_key_exists("statut", lister_tables_objets_sql($table)["field"])) {
				$where[] = "statut='publie'";
			}

			$lignes = sql_allfetsel($champs_autorises, $table, $where);
			foreach ($lignes as $champs) {
				$data[$collection][] = headless_get_objet(objet_type($collection), $champs[id_table_objet($collection)], $champs);
			}
		}
	}

	return $data;
}

function headless_prepareAPIjson($links, $items = "", $erreur = "") {
	$retour = ['collection' => [
		'version' => lire_config("headless_base_version"),
		'href' => url_absolue(self('&')),
	]];

	if (is_array($erreur)) $retour["collection"]['error'] = $erreur;
	if (is_array($links)) $retour["collection"]['links'] = $links;
	if (is_array($items)) $retour["collection"]['items'] = $items;

	return $retour;
}

function headless_prepareAPIreponse($code, $reponse, $json) {
	$reponse->setStatusCode($code);
	$reponse->setCharset('utf-8');
	$reponse->headers->set('Content-Type', 'application/json');
	$reponse->setContent(json_encode($json));

	return $reponse;
}

function headless_get_recherche($table, $recherche, $where = []) {
	include_spip('inc/prepare_recherche');
	include_spip('inc/filtres');

	$champs_exposes = lire_config('/meta_headless/objets_editoriaux/' . table_objet($table) . '/champs');
	$select = [];
	foreach ($champs_exposes as $champ) {
		$select[] = 'collection.' . $champ;
	}

	$prepare_requete = inc_prepare_recherche_dist($recherche, $table);
	$select[] = $prepare_requete[0];
	$from = [$table . ' as collection', table_objet_sql("resultats") .  " as resultats",];
	$where[] = $prepare_requete[1];
	$where[] = 'collection.' . id_table_objet($table) . '=resultats.id';

	return sql_allfetsel($select, $from, $where, '', ['points DESC']);
}


function headless_lancer_build() {
	$last_build = lire_config('/meta_headless/last_build', "");
	$now = headless_getDatetime();
	$last = headless_getDatetime($last_build);
	$interval = $now->diff($last, true);
	$duree = $interval->i + $interval->h * 60 + $interval->d * 24 * 60 + $interval->m * 30 * 24 * 60 + $interval->y * 12 * 30 * 24 * 60;
	$build_limit = lire_config('/meta_headless/build_limit');

	if ($duree > $build_limit) {
		$deploiement = lire_config('/meta_headless/choix_deploiement');
		$output = array();
		$retour = 0;

		switch ($deploiement) {
			case "local":
				if (is_dir(_DIR_RACINE . _DIR_HEADLESS_SRC . "/node_modules")) {
					$cmd = lire_config('/meta_headless/cmd_build_static');

					$commande = 'cd ' . _DIR_RACINE . _DIR_HEADLESS_SRC . " &&  " . $cmd;
					exec($commande, $output, $retour);

					if ($retour !== 0) {
						$ret['message_erreur'] = "<strong>" . _T('headless:error_build_title') . "</strong><br>" . implode("<br>", $output);
					} else {
						$ret['message_ok'] = "<strong>" . _T('headless:success_build_title') . "</strong><br>" . implode("<br>", $output);
					}
				} else {
					$ret['message_erreur'] = _T('headless:error_build_install');
				}
				break;
			case "netlify":
				$netlify_url = lire_config('/meta_headless/netlify_url');

				if ($netlify_url != "") {
					$commande = "curl -X POST -d {} " . $netlify_url;
					exec($commande, $output, $retour);

					if ($retour !== 0) {
						$ret['message_erreur'] = "<strong>" . _T('headless:error_netlify_hook') . "</strong><br>" . implode("<br>", $output);
					} else {
						$ret['message_ok'] = "<strong>" . _T('headless:success_netlify_hook') . "</strong><br>" . implode("<br>", $output);
					}
				} else {
					$ret['message_erreur'] = _T('headless:error_netlify_url');
				}
		}

		if (array_key_exists('message_ok', $ret)) {
			ecrire_config('/meta_headless/last_build', $now->format('Y\-m\-d\ H:i:s'));
			set_request('last_build', $now->format('Y\-m\-d\ H:i:s'));
		}
	} else {
		$ret['message_erreur'] = _T('headless:error_build_timing', array('limit' => $build_limit));
	}

	return $ret;
}
