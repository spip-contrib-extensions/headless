<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// Obligatoire
function headless_autoriser() {
}

function autoriser_configurerheadless_menu_dist($faire, $type, $id, $qui, $opt) {
	return ($qui['webmestre'] == 'oui');
}

function autoriser_headless_construire_dist($faire, $type, $id, $qui, $opt) {
	return ($qui['statut'] == '0minirezo');
}

function autoriser_headless_dist($faire, $type, $id, $qui, $opt) {
	return ($qui['webmestre'] == 'oui');
}
